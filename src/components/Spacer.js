import React from "react";
import { View, StyleSheet} from "react-native";

const Spacer = ({ children, extraStyles }) => {
    const combineStyles = StyleSheet.flatten([extraStyles, styles.spacer])
    return <View style={combineStyles}>{children}</View>
}

const styles = StyleSheet.create({
    spacer:{
        paddingVertical: 10
    }
})

export default Spacer;