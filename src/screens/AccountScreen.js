import React, { useContext } from "react";
import { Context as AuthContext } from '../context/AuthContext';

import { Text, StyleSheet } from "react-native";
import { Button } from "react-native-elements";

import Panel from "../components/Panel";
import Background from "../components/Background";
import {FontAwesome} from "@expo/vector-icons";

const AccountScreen = () => {
    const { signout } = useContext(AuthContext);

    return (
        <Background extraStyles={{ justifyContent: "flex-start" }} >
            <Panel>
                <Text style={styles.style1}>Your Account</Text>
            </Panel>
            <Panel>
                <Button style={styles.button} title="Log out" onPress={signout} />
            </Panel>
        </Background>
    )
}

AccountScreen.navigationOptions = {
    title: 'Account',
    tabBarIcon: <FontAwesome name="gears" size={24} color="black" />
}

const styles = StyleSheet.create({

    style1: {
        fontSize: 30,
        color: "white",
        alignSelf: "center",
    },
    button: {
        width: "100%"
    }
})

export default AccountScreen;