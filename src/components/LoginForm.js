import React, {useState} from "react";
import {StyleSheet} from "react-native";
import { Button, Text, Input } from "react-native-elements";
import CenterAlign from "../components/CenterAlign";

import NavLink from "./NavLink";


const LoginForm = ({ title, cta, navigation, backLink, link, onSubmit, initialVals, errorMessage }) => {
    const [email, setEmail] = useState(initialVals.email);
    const [password, setPassword] = useState(initialVals.password);
    console.log(errorMessage);

    const message = (typeof errorMessage === 'object' && errorMessage !== null) ? errorMessage.error : errorMessage;


    return (
            <>

                <CenterAlign extraStyles={{ marginBottom: 20 }} >
                    <Text h4 style={styles.style1}>
                        { title }
                    </Text>
                </CenterAlign>
                <Input
                    leftIcon={{ type: 'font-awesome', name: 'envelope', color: 'white', size: 20   }}
                    autoCapitalize="none"
                    autoCorrect={false}
                    labelStyle={styles.style1}
                    label="Email"
                    placeholder="Your email address"
                    value={email}
                    onChangeText={setEmail}
                    color="white"
                    inputStyle={{ paddingLeft: 10 }}
                />
                <Input
                    leftIcon={{ type: 'font-awesome', name: 'lock', color: 'white' }}
                    autoCapitalize="none"
                    autoCorrect={false}
                    labelStyle={styles.style1}
                    label="Password"
                    placeholder="Make it secure"
                    value={password}
                    onChangeText={setPassword}
                    secureTextEntry
                    color="white"asd
                    inputStyle={{ paddingLeft: 15 }}
                />
                { message ? <Text  style={styles.error } >{message}</Text> : null}
                <Button style={styles.button}
                        title={cta}
                        onPress={() => {
                            onSubmit(email, password)
                        }}
                />
                <NavLink link={link} backLink={backLink} />
            </>
    )
}

LoginForm.defaultProps = {
    initialVals: {
        email: '',
        password: ''
    }
}

const styles = StyleSheet.create({
    button:{
        padding: 10
    },
    container:{
        padding: 0
    },
    style1: {
        color: 'white',
    },
    cta:{
        color: 'white',
        padding: 10
    },
    error: {
        fontSize: 12,
        color: 'white'
    }
})

export default LoginForm;