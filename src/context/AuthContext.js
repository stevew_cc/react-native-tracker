import createDataContext from "./createDataContext";
import trackerAPI from "../api/tracker";
import AsyncStorage from '@react-native-community/async-storage';
import { navigate } from "../navigationRef";

import { ADD_ERROR, ADD_TOKEN, CLEAR_ERROR, DEL_TOKEN} from "./types"

const authReducer = (state, action) => {
    switch(action.type){
        // case UPDATE_AUTH:
        //     return {
        //         email: action.payload.email,
        //         password: action.payload.password
        //     }
        case ADD_ERROR:
            return {...state,
                errorMessage: action.payload,
            }
        case CLEAR_ERROR:
            return {...state,
                errorMessage: '',
            }
        case ADD_TOKEN:
            return {errorMessage: '', token: action.payload }
        case DEL_TOKEN:
            return {errorMessage: '', token: null }
        default:
            return state;
    }
};

// TODO: No Longer used remove
// const setAuth = dispatch => {
//     return (email, password, callback) => {
//         dispatch({
//             type: UPDATE_AUTH,
//             payload: {
//                 email,
//                 password
//             }
//         });
//         if(callback){
//             callback();
//         }
//     }
// }

const tryLocalSignin = dispatch => async () =>{
    const token = await AsyncStorage.getItem('token');
    console.log("token: ", token);
    if(token){
        dispatch({ type: ADD_TOKEN, token });
        navigate('TrackList');
    }else{
        navigate('loginFlow');
    }
}

const clearErrorMessage = dispatch => () => {
    dispatch({ type: CLEAR_ERROR })
}

const signup =  dispatch => async (email, password) => {
    try{
        const response = await trackerAPI.post('/signup', { email, password });
        await AsyncStorage.setItem('token', response.data.token );
        dispatch( { type: ADD_TOKEN, payload: response.data.token });
        navigate('TrackList');

    }catch (e) {
        dispatch( { type: ADD_ERROR, payload: e.response.data });
    }
}

const signin = dispatch => async (email, password) => {

    try {
        const response = await trackerAPI.post('/signin', {email, password});
        try{
            console.log(response.data);
            await AsyncStorage.setItem('token', response.data.token );
            dispatch( { type: ADD_TOKEN, payload: response.data.token });
            navigate('TrackList');
        }catch (e) {
            dispatch( { type: ADD_ERROR, payload: "Storage error: " + e });
        }

    }catch(e){
        if(e.response){
            dispatch( { type: ADD_ERROR, payload: "Auth Error: " + e.response.data.error });
        }else{
            dispatch( { type: ADD_ERROR, payload: "Axios " + e});
        }

    }



}

const signout = dispatch => async () => {
    console.log("Sign out");
    await AsyncStorage.removeItem('token');
    dispatch({ type: DEL_TOKEN});
    navigate('loginFlow');
}

export const  { Context, Provider } = createDataContext(
    authReducer,
    { signin, signout, signup, clearErrorMessage, tryLocalSignin},
    { token: false, errorMessage: '' }
    );