import React from "react";
import { ImageBackground, StyleSheet} from "react-native";
import {SafeAreaView} from "react-navigation";

const Background = ({ children, extraStyles }) => {
    const combineStyles = StyleSheet.flatten([extraStyles, styles.image])

    return (

        <ImageBackground source={require('../../assets/Background98.png')} style={combineStyles}>
            <SafeAreaView forceInset={{top: 'always'}}>

                { children }
            </SafeAreaView>
        </ImageBackground>

    )
}

const styles = StyleSheet.create({
    image: {
        flex: 1,
        resizeMode: "cover"
    }
})


export default Background;