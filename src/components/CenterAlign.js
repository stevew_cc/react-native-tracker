import React from "react";
import { Text, View, StyleSheet} from "react-native";

const CenterAlign = ({ children, extraStyles }) => {
    const combineStyles = StyleSheet.flatten([extraStyles, styles.center])
    return (

            <View style={combineStyles}>{ children }</View>


    )
}

const styles = StyleSheet.create({
    center:{
        justifyContent: "center",
        alignSelf: "center",
    }
})

export default CenterAlign;