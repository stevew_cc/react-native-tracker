import createDataContext from './createDataContext';
import trackerAPI from "../api/tracker";

import {TRACKS_GET} from "./types";


const tracksReducer = (state, action) => {
    switch (action.type) {
        case TRACKS_GET:
            return action.payload;
        default:
            return state;
    }
};


const fetchTracks = dispatch => async () => {
    const response = await trackerAPI.get('/tracks');
    dispatch( { type: TRACKS_GET, payload: response.data })
}

const createTrack = dispatch => async (name, locations) => {
    await trackerAPI.post('/tracks', { name, locations });
}

const deleteTrack = dispatch => async (track) => {
    console.log("Delete Track: " + track );
    await trackerAPI.delete('/track', {params: { id: track }});
    console.log("Track deleted" );
}


export const { Context, Provider } = createDataContext(
    tracksReducer,
    { fetchTracks, createTrack, deleteTrack },
    {
        tracks: []
    }
);