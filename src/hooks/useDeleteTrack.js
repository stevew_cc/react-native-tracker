import { useContext } from 'react';
import { Context as TrackContext } from "../context/TrackContext";
import { navigate } from "../navigationRef";

export default (id) => {
  const { deleteTrack, fetchTracks } = useContext(TrackContext);

  const delTrack = async () => {
      await deleteTrack(id);
      navigate('TrackList');
  };

  return [delTrack];
};