import React, { useContext } from "react";
import LoginForm from "../components/LoginForm";
import { Context as AuthContext } from "../context/AuthContext";
import Panel from "../components/Panel";
import Background from "../components/Background";
import {NavigationEvents} from "react-navigation";
import {StyleSheet, View} from "react-native";
import LogoSVG from "../../assets/ilyo.svg";


const SignupScreen = ({ navigation }) => {
    const { state, signup, clearErrorMessage } = useContext(AuthContext);

    return (
        <Background extraStyles={{ justifyContent: "center" }}>
            <Panel>
                <NavigationEvents
                    onWillFocus={clearErrorMessage}
                />
                <View style={styles.logo} ><LogoSVG width={200} height={100}/></View>
            <LoginForm
                title="Sign up for Tracker"
                cta="Sign up"
                errorMessage={state.errorMessage}
                backLink="Aleady have an account sign in here?"
                link="Signin"
                onSubmit={(email, password) => {
                    signup(email, password);
                }}
            />
            </Panel>
        </Background>
    )

}


const styles = StyleSheet.create({
    logo: {

        alignSelf: "center",
        marginBottom: 10
    },
});

SignupScreen.navigationOptions = () => {
    return {
        header: () => false,
    };
};
export default SignupScreen;