import React, { useContext } from "react";
import { NavigationEvents } from "react-navigation";
import LoginForm from "../components/LoginForm";
import Panel from "../components/Panel";
import Background from "../components/Background";
import { Context as AuthContext } from "../context/AuthContext";
import LogoSVG from '../../assets/ilyo.svg'
import { View, StyleSheet} from "react-native";

const SigninScreen = ({ navigation }) => {
    const { state, signin, clearErrorMessage } = useContext(AuthContext);



    return (
        <Background extraStyles={{ justifyContent: "center" }}>
            <Panel>
                <NavigationEvents
                    onWillFocus={clearErrorMessage}
                />
                <View style={styles.logo} ><LogoSVG width={200} height={100}/></View>
                <LoginForm
                    title="Login to Tracker"
                    cta="Sign In"
                    backLink="Get an account here?"
                    link="Signup"
                    onSubmit={(email, password) => {
                        signin(email, password);
                    }}
                    errorMessage={state.errorMessage}
                />
            </Panel>
        </Background>

    )
}

const styles = StyleSheet.create({
    logo: {

        alignSelf: "center",
        marginBottom: 10
    },
});

SigninScreen.navigationOptions = () => {
    return {
        header: () => false,
    };
};

export default SigninScreen;