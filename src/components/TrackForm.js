import React, { useContext } from "react";
import { StyleSheet } from "react-native";
import { Button, Input } from "react-native-elements";
import { Context as LocationContext } from "../context/LocationContext";
import Spacer from "./Spacer";
import useSaveTrack from "../hooks/useSaveTrack";

const TrackForm = () => {
    const {
        state: { name, recording, locations },
        startRecording,
        stopRecording,
        changeName
    } = useContext(LocationContext);
    const [saveTrack] = useSaveTrack();



    console.log("No Locs: " + locations.length);
    return (
        <>

            <Input
                value={name}
                onChangeText={changeName}
                color="white"
                placeholder="Enter Track Name"


            />

            {recording
                ? <Button title="Stop Recording" buttonStyle={{ backgroundColor: "red"}} onPress={stopRecording} />
                : <Button title="Start Recording" buttonStyle={{ backgroundColor: "green"}} onPress={startRecording} />
            }
            <Spacer extraStyles={{ marginVertical: 0 }} />

            {
                !recording && locations.length
                ? <Button title="Save Recording" onPress={saveTrack} />
                : null
            }

        </>
    )
}


const styles = StyleSheet.create({
    style1: {}
})

export default TrackForm;