// import '../_mockLocation';

import React, { useContext, useCallback } from "react";
import { StyleSheet } from "react-native";
import { Text } from "react-native-elements";
import { withNavigationFocus } from "react-navigation";

import { Context as LocationContext } from "../context/LocationContext"

import Map from "../components/Map";
import TrackForm from "../components/TrackForm";
import useLocation from "../hooks/useLocation";

import Panel from "../components/Panel";
import Background from "../components/Background";
import TrackListScreen from "./TrackListScreen";
import { FontAwesome } from '@expo/vector-icons';
import MySVGImage from '../../assets/ilyo.svg'

const TrackCreateScreen = ({ isFocused }) => {
    const { state: { recording }, addLocation } = useContext(LocationContext);
    const callback = useCallback(
        location => {
            //console.log("Inside: " + recording);
            addLocation(location, recording);
        },
        [recording]
    );
    const [err] = useLocation(isFocused || recording , callback);
    //console.log("Outside: " + recording);
    //console.log("isFocused: " + isFocused);
    return (
        <Background>
            <Panel>

                <Text h3 style={styles.style1}>
                    Track your routes
                </Text>
            </Panel>
                <Map />
            <Panel>
                { err ? <Text style={styles.err} >{ err }</Text> : null }
                <TrackForm />
            </Panel>


        </Background>
    )
}


TrackCreateScreen.navigationOptions = {
    title: 'Add Track',
    tabBarIcon: <FontAwesome name="plus" size={24} color="black" />
}

const styles = StyleSheet.create({
    style1: {
        color: "white",
        fontSize: 30,
        textAlign: "center"
    },
    error: {
        fontSize: 14,
        color: "white",
        paddingBottom: 10
    }
})

export default withNavigationFocus(TrackCreateScreen);