import createDataContext from './createDataContext';
import { ADD_CURRENT_LOC, ADD_LOC, ADD_REC, REC_STOP, REC_START, TRACK_NAME, RESET} from "./types"

const locationReducer = (state, action) => {
  switch (action.type) {
    case ADD_CURRENT_LOC :
      return { ...state, currentLocation: action.payload};
    case REC_START :
        return { ...state, recording: true };
    case REC_STOP :
        return { ...state, recording: false };
    case ADD_LOC :
        return { ...state, locations: [...state.locations, action.payload] };
    case TRACK_NAME :
        return { ...state, name: action.payload };
    case RESET :
      return { ...state, name: '', locations: [] };
    default:
      return state;
  }
};

const reset = dispatch => () =>{
    dispatch({ type: RESET })
}

const changeName = dispatch => (name) => {
  dispatch({ type: TRACK_NAME, payload: name })
}

const startRecording = dispatch => () => {
  dispatch({ type: REC_START })
};
const stopRecording = dispatch => () => {
  dispatch({ type: REC_STOP })
};

const addLocation = dispatch => (location, recording) => {
    dispatch({ type: ADD_CURRENT_LOC, payload: location });
    if (recording) {
        dispatch({ type: ADD_LOC, payload: location });
    }
};

export const { Context, Provider } = createDataContext(
  locationReducer,
  { startRecording, stopRecording, addLocation, changeName, reset },
  {
      name: '', recording: false, locations: [], currentLocation: null
  }
);
