import React, { useContext } from "react";
import {  StyleSheet, View } from "react-native";
import { Text, Button } from "react-native-elements";
import Panel from "../components/Panel";
import Background from "../components/Background";
import { Context as TrackContext } from '../context/TrackContext';
import MapView, {Circle, Polyline} from "react-native-maps";
import useDeleteTrack from "../hooks/useDeleteTrack";



const TrackDetailScreen = ({ navigation }) => {
    const { state, deleteTrack } = useContext(TrackContext);
    const _id = navigation.getParam('_id');
    const [delTrack] = useDeleteTrack(_id);

    const track = state.find(t => t._id === _id );
    const initalCoords = track.locations[0].coords

    return (
        <Background extraStyles="center" >
            <Panel>
                <View>
                    <Text style={styles.style1}>
                        Track Details:
                    </Text>
                    <Text style={styles.style2}>
                        {track.name}
                    </Text>

                </View>
            </Panel>
            <MapView
                style={styles.map }
                initialRegion={{
                    ...initalCoords,
                    latitudeDelta: 0.01,
                    longitudeDelta: 0.01,
                }}
            >
                <Polyline coordinates={track.locations.map( loc => loc.coords )}  />
            </MapView>
            <Panel>
                <Button title="Delete Track"  buttonStyle={{ backgroundColor: "red"}} onPress={delTrack}/>
            </Panel>

        </Background>
    )
}

TrackDetailScreen.navigationOptions = {
    title: null,
}



const styles = StyleSheet.create({
    style1: {
        fontSize: 30,
        color: "white",
        textAlign: 'center'
    },
    style2: {
        fontSize: 20,
        color: "white",
        textAlign: 'center'
    },
    map: {
        height: 300,
    }
})

export default TrackDetailScreen;