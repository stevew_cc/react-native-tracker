import React from 'react';

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import AccountScreen from "./src/screens/AccountScreen";
import SigninScreen from "./src/screens/SigninScreen";
import SignupScreen from "./src/screens/SignupScreen";
import TrackCreateScreen from "./src/screens/TrackCreateScreen";
import TrackDetailScreen from "./src/screens/TrackDetailScreen";
import TrackListScreen from "./src/screens/TrackListScreen";
import ResolveAuthScreen from "./src/screens/ResolveAuthScreen";

import {createBottomTabNavigator} from "react-navigation-tabs";
import { setNavigator } from "./src/navigationRef";

import { Provider as AuthContext } from "./src/context/AuthContext";
import { Provider as LocationContext } from "./src/context/LocationContext";
import { Provider as TrackProvider } from "./src/context/TrackContext"
import {FontAwesome} from "@expo/vector-icons";

const trackListFlow = createStackNavigator({
  TrackList: TrackListScreen,
  TrackDetail: TrackDetailScreen
})

trackListFlow.navigationOptions = {
  title: 'Tracks',
  tabBarIcon: <FontAwesome name="th-list" size={24} color="black" />
}

const switchNavigator = createSwitchNavigator({
  ResolveAut: ResolveAuthScreen,
  loginFlow: createStackNavigator({
    Signin: SigninScreen,
    Signup: SignupScreen,
  }),
  mainFlow: createBottomTabNavigator({
    trackListFlow,
    TrackCreate: TrackCreateScreen,
    Account: AccountScreen,

  }),

})

const App = createAppContainer(switchNavigator);

export default () => {
  return (
      <TrackProvider>
        <LocationContext>
          <AuthContext>
            <App ref={(navigator) => { setNavigator(navigator) }} />
          </AuthContext>
        </LocationContext>
      </TrackProvider>
  )
}

