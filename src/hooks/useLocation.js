import { useState, useEffect } from 'react';
import {
    Accuracy,
    requestPermissionsAsync,
    watchPositionAsync,
} from 'expo-location';

export default (shouldTrack, callback) => {
    const [err, setErr] = useState(null);
    const [subscriber, setsubscriber] = useState(null);

    useEffect(() => {

        const startWatching = async () => {
            try {
                const { granted } = await requestPermissionsAsync();
                if (!granted) {
                    throw new Error('Location permission not granted');
                }
                const sub = await watchPositionAsync(
                    {
                        accuracy: Accuracy.BestForNavigation,
                        timeInterval: 1000,
                        distanceInterval: 10,
                    },
                    callback
                );
                setsubscriber(sub);
            } catch (e) {
                setErr(e);
            }
        };

        if (shouldTrack) {
            console.log("Should Track");
            startWatching();
        } else {
            console.log("Stop Track");
            if (subscriber) {
                console.log("Remove");
                subscriber.remove();
            }
            setsubscriber(null);
        }

        return () => {
            if (subscriber) {
                subscriber.remove();
            }
        };
    }, [shouldTrack, callback]);

    return [err];
};
