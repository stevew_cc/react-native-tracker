import React from "react";
import { Text, TouchableOpacity, StyleSheet} from "react-native";
import { withNavigation } from "react-navigation";

const NavLink  = ({ navigation, link, backLink }) =>{
    return (
        <TouchableOpacity onPress={() => navigation.navigate(`${link}`)}>
            <Text style={styles.cta} >{backLink}</Text>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    cta:{
        color: 'white',
        padding: 10
    }
});

export default withNavigation(NavLink)