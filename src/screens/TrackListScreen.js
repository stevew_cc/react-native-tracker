import React, { useContext } from "react";
import { StyleSheet, View, Button, FlatList, TouchableOpacity, SafeAreaView, ScrollView } from "react-native";
import { Text, ListItem } from "react-native-elements";
import Background from "../components/Background";
import Panel from "../components/Panel";
import Spacer from "../components/Spacer";
import { Context as TrackContext } from "../context/TrackContext"
import { NavigationEvents } from "react-navigation";

const TrackListScreen = ({ navigation }) => {
    const { state, fetchTracks } = useContext(TrackContext);

    const keyExtractor = (item, index) => index.toString()

    const renderItem = ({ item }) => (
        <TouchableOpacity onPress={() => navigation.navigate('TrackDetail', { _id: item._id}) }>
            <ListItem bottomDivider containerStyle={{ backgroundColor: "rgba(255,255,255,0.5)", marginBottom: 2 }}>
                <ListItem.Content>
                    <ListItem.Title style={styles.title}>{item.name}</ListItem.Title>
                </ListItem.Content>
                <ListItem.Chevron color="white" />
            </ListItem>
        </TouchableOpacity>
    )

    return (
        <Background extraStyles="center" >
            <NavigationEvents onWillFocus={fetchTracks} />
            <Panel>
                <View>
                    <Spacer/>
                    { state.length > 0
                        ? <FlatList style={styles.list} data={state} keyExtractor={item => item._id} renderItem={renderItem}  />
                        : <><Text style={styles.text}  h2>No Tracks?</Text><Text style={styles.text}>Sorry you do not have any tracks recorded yet.</Text><Text style={styles.text}>Click on the "Add Track" button below</Text></>
                    }





                </View>
            </Panel>
        </Background>
    )
}

TrackListScreen.navigationOptions = {
    title: 'Tracks'
}


const styles = StyleSheet.create({
    style1: {
        fontSize: 30,
        color: "white",
        textAlign: 'center'

    },
    title: {
        color: 'white',
        fontWeight: 'bold'
    },
    list: {
        height: 450
    },
    text: {
        textAlign: "center",
        color: "white"
    }

})


export default TrackListScreen;